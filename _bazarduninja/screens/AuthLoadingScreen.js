import React from 'react';
import {
    ActivityIndicator,
    // AsyncStorage,
    StatusBar,
    StyleSheet,
    View,
} from 'react-native';

export class AuthLoadingScreen extends React.Component {

    constructor(props)
    {
        super(props);
        // this._bootstrapAsync();
        // const userToken = true ;
    };


    // props.navigation.navigate( true ? 'App' : 'Auth');

    // Render any loading content that you like here
    render() {
        return (
            <View >
                <ActivityIndicator />
                <StatusBar barStyle="default" />
            </View>
        );
    }
}