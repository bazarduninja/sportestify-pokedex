import React, { Component } from 'react';

import { createSwitchNavigator, createStackNavigator } from 'react-navigation';

import { HomeScreen} from "./_bazarduninja/screens/HomeScreen";
import { AuthLoadingScreen } from "./_bazarduninja/screens/AuthLoadingScreen";

const AppStack = createStackNavigator({ Home: HomeScreen });
const AuthStack = createStackNavigator({ SignIn: AuthLoadingScreen });

const ModalNavigator =  createSwitchNavigator(
    {
        AuthLoading: AuthLoadingScreen,
        App: AppStack,
        Auth: AuthStack,
    },
    {
        initialRouteName: 'App',
    }
);

export default class App extends Component
{
  render()
  {
    return (
      <ModalNavigator/>
    );
  }
}
